import java.util.Scanner;

public class lab5 
{
	public static void main(String[] args) 
	{
		// Variable Declaraton
		int num, minimum, maximum, i, count, sum = 0;
		
		// Create an object of Scanner class
		Scanner sc = new Scanner(System.in);
		
		// Take input from users
		System.out.print(" Please Enter the Minimum value : ");
		minimum = sc.nextInt();	
		
		System.out.print(" Please Enter the Maximum value : ");
		maximum = sc.nextInt();	
		
		// logic implementation
		for(num = minimum; num <= maximum; num++)
		{
			count = 0;
		    for (i = 2; i <= num/2; i++)
		    {
		    	if(num % i == 0)
		    	{
		    		count++;
		    		break;
		    	}
		    }
		    if(count == 0 && num != 1 )
		    {
                sum += num;
		    }  
		}
		
		// Display Result
        System.out.println("\n The Sum of Prime Numbers = " + sum);
	}
}