import java.util.Scanner;

class lab2
{
	public static void main(String[] args)
	{
		
		// create an object of Scanner class
		Scanner sc = new Scanner(System.in);
		
		// take input from users
		System.out.print("Enter first number : ");
		int num1 = sc.nextInt();
		System.out.print("Enter second number : ");
		int num2 = sc.nextInt();
		
		// display result before swapping
		System.out.println("Numbers before swapping : " + num1 + " " + num2);
		
		// logic implementaton
		int temp = num1;
		num1 = num2;
		num2 = temp;
		
		// display result after swapping
		System.out.println("Numbers after swapping : " + num1 + " " + num2);

	}
}