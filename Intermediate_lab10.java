import java.util.Scanner;

public class Intermediate_lab10
{
    public static void main(String[] args) 
	{
		// Declare & initialize Variable
        int count, num1 = 0, num2 = 1;
		
		// Create Scanner Object to take user input
		Scanner sc = new Scanner(System.in);
		
		// Taking user Input
        System.out.println("Numbers you want in the sequence: ");
        count = sc.nextInt();
		
		// Logic implementation
        int i=1;
        while(i<=count)
        {
            System.out.print(num1+" ");
            int sumOfPrevTwo = num1 + num2;
            num1 = num2;
            num2 = sumOfPrevTwo;
            i++;
        }
		
		// Display Result
		System.out.print("Fibonacci Series of "+count+" numbers :");
    }
}