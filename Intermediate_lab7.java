import java.util.Scanner;
public class Intermediate_lab7  
{  
	public static void main(String[] args)   
	{
		// Variable decleration and initialization
		int sum = 0;
		
		// Create scanner object to take user input
		Scanner sc = new Scanner(System.in);
		
		// Crating an array 
		int[] array = new int[10];
		
		// Taking user input
		System.out.println("Enter the elements:");
		
		// Logic implementation
		for (int i=0; i<10; i++)
		{
			array[i] = sc.nextInt();
		}
		for( int num : array) 
		{
			sum = sum+num;
		}
		
		// Display result
		System.out.println("Sum of array elements is:"+sum);
   }
}