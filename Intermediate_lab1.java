import java.util.Scanner;
class Intermediate_lab1 
{
	public static void main(String[] args)
	{
		long result = 1;
	
		Scanner sc = new Scanner(System.in);
	
		System.out.println("Enter the number : ");
		int num = sc.nextInt();
	
		System.out.println("Enter the power : ");
		int pow = sc.nextInt();

		for (; pow != 0; --pow) 
		{
			result *= num;
		}

		System.out.println("Result = " + result);
	}
}
