import java.util.Scanner;
class Intermediate_lab2
{
   public static void main(String args[])
   {
	  // Declare Variables
      int i, first, last, middle;
	  
      // Create Scanner Object to take user input
      Scanner sc = new Scanner(System.in);
	  
	  // Taking user Input
      System.out.println("Enter number of elements:");
      int num = sc.nextInt(); 

      //Creating an array to store all the elements
      int array[] = new int[num];

      System.out.println("Enter " + num + " integers");
	  
      //Loop to store each numbers in array
      for (i=0; i<num; i++)
	  {
		  array[i] = sc.nextInt();
	  }
		
      System.out.println("Enter the search value:");
      int item = sc.nextInt();
	  
      first = 0;
      last = num - 1;
      middle = (first + last)/2;

      while(first <= last)
      {
         if (array[middle] < item)
			 first = middle + 1;
         else if (array[middle] == item)
         {
           System.out.println(item + " found at location " + (middle + 1));
           break;
         }
         else
         {
             last = middle - 1;
         }
         middle = (first + last)/2;
      }
      if (first > last)
          System.out.println(item + " is not found.");
   }
}