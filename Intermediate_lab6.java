public class Intermediate_lab6 
{
    public static void main(String[] args) 
	{
		// Declaring & initializing a variable
		double total = 0;

		// Creating and initializing an Array
        double[] arr = {53, 45, 29.8, 103, 33.71};
        
		// Iterating the elements in an array
        for(int i=0; i<arr.length; i++){
        	total = total + arr[i];
        }
		
		// logic implementation
        double average = total / arr.length;
        
		// Display result
        System.out.format("The average is : " + average);
    }
}