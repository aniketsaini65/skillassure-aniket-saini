import java.util.Scanner;

public class Intermediate_lab8
{
    public static void main(String args[])
    {
		// Declare & initialize Variable
		int num, start, end, i, rem, temp, count=0;
		
		// Create Scanner Object to take user input
		Scanner sc = new Scanner(System.in);
		
		// Taking user Input
		System.out.print("Enter the start number: ");
		start = sc.nextInt();
		System.out.print("Enter the end number: ");
		end = sc.nextInt();

		// Logic implementation
		for(i=start+1; i<end; i++)
		{
			temp = i;
			num = 0;
			while(temp != 0)
			{
				rem = temp%10;
				num = num + rem*rem*rem;
				temp = temp/10;
			}
			if(i == num)
			{
				if(count == 0)
				{
					System.out.print("Armstrong Numbers Between "+start+" and "+end+": ");
				}
				System.out.print(i + "  ");
				count++;
			}
		}
		
		// If no Armstrong number is found
		if(count == 0)
		{
			System.out.print("There is no Armstrong number Between "+start+" and "+end);
		}
    }
}