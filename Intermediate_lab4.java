class Intermediate_lab4
{
   public static void main (String[] args)
   {
		// Declare & initialize Variable
		int i = 0, num = 0;
	   
       // Create Empty String
       String  primeNumbers = "";
	   
	   // Logic Implementation
       for (i = 1; i <= 100; i++)         
       {
			int count=0; 	  
			for(num=i; num>=1; num--)
			{
				if(i%num == 0)
				{
					count += 1;
				}
			}
			if (count == 2)
			{
				//Appended the Prime number to the String
				primeNumbers = primeNumbers + i + " ";
			}	
       }
	   
	   // Display Result
       System.out.print("Prime numbers from 1 to 100 are : " + primeNumbers);
   }
}