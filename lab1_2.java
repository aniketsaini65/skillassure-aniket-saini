import java.util.Scanner;

public class lab1_2 {

    public static void main(String[] args) {
	
		// create an object of Scanner class
        Scanner reader = new Scanner(System.in);

		// take input from users
        System.out.print("Enter a number: ");
        int num = reader.nextInt();

		// logic implementation
        if(num % 2 == 0)
			//Display result
            System.out.println(num + " is even");
        else
            System.out.println(num + " is odd");
    }
}