import java.util.*;

public class lab7 
{
    public static void main(String[] args) 
	{
	
		// create an object of Scanner class
        Scanner sc = new Scanner(System.in);

		// take input from users
        System.out.print("Enter the string: ");
        String s = sc.nextLine();

		// logic implementation
		StringTokenizer st = new StringTokenizer(s, " ");
		
		while(st.hasMoreTokens() == true)
		{
			System.out.println(st.nextToken());
		}
	}
}