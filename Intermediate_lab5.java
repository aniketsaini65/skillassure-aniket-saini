import java.util.Scanner;
class Intermediate_lab5
{
   public static void main (String[] args)
   {
		// Declare a boolean variable
		boolean isVowel=false;
		
		// create a scanner object for input
		Scanner sc = new Scanner(System.in);
		
		// taking input from user
		System.out.println("Enter a character : ");
		char ch = sc.next().charAt(0); 
		
		// logic implementation
		switch(ch)
		{
			//check lower case vowel letters
			case 'a' :
			case 'e' :
			case 'i' :
			case 'o' :
			case 'u' :
			
			//check upper case vowel letters
			case 'A' :
			case 'E' :
			case 'I' :
			case 'O' :
			case 'U' : 
			isVowel = true;
		}
		if(isVowel == true) 
		{
			System.out.println(ch + " is  a Vowel");
		}
		else 
		{
			if((ch>='a' && ch<='z') || (ch>='A' && ch<='Z'))
				System.out.println(ch + " is a Consonant");
			else
				System.out.println(ch + " is not an alphabet");		
        }
   }
}