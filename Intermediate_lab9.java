import java.util.Scanner;
public class Intermediate_lab9 
{
    public static void main(String[] args) 
    {
		// Declare Variable
        int num1, num2, num3, result, temp;
        
		// Create Scanner Object to take user input		
        Scanner sc = new Scanner(System.in);
		
		// Taking user Input
        System.out.println("Enter First Number:");
        num1 = sc.nextInt();
        System.out.println("Enter Second Number:");
        num2 = sc.nextInt();
        System.out.println("Enter Third Number:");
        num3 = sc.nextInt();
        
		// Logic implementation
        temp = num1 < num2 ? num1:num2;
        result = num3 < temp ? num3:temp;
		
		// Display result
        System.out.println("Smallest Number is:"+result);
    }
}